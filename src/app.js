const { MiBoxConnection } = require("milight-wifi-box");
const net = require("net");

let config = {
	miBoxIp: "192.168.1.5",
	miBoxPort: undefined,
	prismatikApiKey: "{31241e7d-5ed0-4447-856a-d28ec35e1c38}",
	lightMap: {
		"0": [{
			group: 1,
			lightType: "rgbww",
		}],
		// "1": [{
		// 	group: 3,
		// 	lightType: "rgbww",
		// }]
	}
}

config = {...{
	prismatikIp: "127.0.0.1",
	prismatikPort: 3636,
	updateInterval: 100,
}, ...config}

let miConnection = new MiBoxConnection(config.miBoxIp, config.miBoxPort);

let initialPrismatikConnectionDone = false;
const prismatikClient = new net.Socket();
prismatikClient.connect(config.prismatikPort, config.prismatikIp, async _ => {
	let connectResult = await sendPrismatikCmd();
	console.log("connected to prismatik");
	initialPrismatikConnectionDone = true;
	let result = await sendPrismatikCmd("apikey:"+config.prismatikApiKey);
	if(!result.startsWith("ok")){
		console.log("failed to connect to prismatik, invalid api key?");
	}
	getLightsLoop();
});
let onPrismatikRecvCbs = [];
prismatikClient.on("data", data => {
	for(const cb of onPrismatikRecvCbs){
		cb(data.toString());
	}
	onPrismatikRecvCbs = [];
});
prismatikClient.on("error", e => {
	if(!initialPrismatikConnectionDone){
		console.log("error connecting to prismatik, make sure it's running and you have enabled the server api in the experimental tab");
	}else{
		throw e;
	}
});

async function sendPrismatikCmd(cmd){
	let promise = new Promise(r => onPrismatikRecvCbs.push(r));
	if(cmd)	prismatikClient.write(cmd+"\n");
	return await promise;
}

let lastReceivedColors = {};

async function getLightsLoop(){
	while(true){
		let result = await sendPrismatikCmd("getcolors");
		if(result.startsWith("colors:")){
			let colorsStr = result.substring(7);
			let splitColors = colorsStr.split(";");
			for(const color of splitColors){
				let match = color.match(/(\d+)-(\d+),(\d+),(\d+)/);
				if(match){
					lastReceivedColors[match[1]] = [
						parseInt(match[2]),
						parseInt(match[3]),
						parseInt(match[4]),
					];
				}
			}
		}
	}
}

setInterval(_ => {
	for(const [key, value] of Object.entries(lastReceivedColors)){
		let mappedLights = config.lightMap[key];
		if(mappedLights){
			for(const light of mappedLights){
				miConnection.setRgb(...value, light.lightType, light.group);
			}
		}
	}
}, config.updateInterval);
